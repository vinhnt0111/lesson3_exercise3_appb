package gst.trainingcourse.lesson3_ex3_b

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class MyBroadCastReceive : BroadcastReceiver() {
    override fun onReceive(context: Context?, p1: Intent?) {
        val myIntent = Intent(context?.applicationContext, MainActivity::class.java)
        myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context?.applicationContext?.startActivity(myIntent)
    }
}